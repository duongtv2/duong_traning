<?php

namespace Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Realse extends Model
{
    use Notifiable;
    protected $table = 'realse_numbers';
    protected $fillable = [
        'name','description','price','image_id'
    ];

    public function image() {
        return $this-> belongsTo('Backend\Models\Image','image_id','id');
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }

}
