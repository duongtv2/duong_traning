<?php

namespace Backend\Providers;

use Backend\Repositories\Contracts\CategoryRepositoryInterface;
use Backend\Repositories\Contracts\PostRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Backend\Repositories\Contracts\ReleaseRepositoryInterface',
            'Backend\Repositories\Eloquents\ReleaseRepository'
        );
        $this->app->bind(
            'Backend\Repositories\Contracts\ImageRepositoryInterface',
            'Backend\Repositories\Eloquents\ImageRepository'
        );
        $this->app->bind(
            'Backend\Repositories\Contracts\PostRepositoryInterface',
            'Backend\Repositories\Eloquents\PostRepository'
        );
        $this->app->bind(
            'Backend\Repositories\Contracts\CategoryRepositoryInterface',
            'Backend\Repositories\Eloquents\CategoryRepository'
        );
    }
}
