<?php
namespace Backend\Repositories\Contracts;

interface CategoryRepositoryInterface
{
    public function get_all();
    public function create(array $input);
    public function getbyid($id);
}