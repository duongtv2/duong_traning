<?php
namespace Backend\Repositories\Contracts;

interface ImageRepositoryInterface
{
    public function create(array $input);
    public function find($id);
    public function update(array $input, $id);
    public function delete($id);
}