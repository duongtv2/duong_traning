<?php
// app/Repositories/Contracts/ProductRepositoryInterface.php

namespace Backend\Repositories\Contracts;

interface ReleaseRepositoryInterface
{
    public function all();
    public function find($id);
    public function create(array $input);
    public function storage($file);
    public function edit($id);
    public function update(array $input, $id);
    public function delete($id);
}