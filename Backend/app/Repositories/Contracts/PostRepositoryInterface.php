<?php
namespace Backend\Repositories\Contracts;

interface PostRepositoryInterface
{
    public function get_all();
    public function create(array $input);
    public function edit($id);
    public function update(array $input, $id);
    public function delete($id);
}