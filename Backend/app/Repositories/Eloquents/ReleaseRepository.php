<?php
// app/Repositories/Eloquents/ProductRepository.php

namespace Backend\Repositories\Eloquents;

use Backend\Models\Realse;
use Backend\Repositories\Contracts\ReleaseRepositoryInterface;
use Illuminate\Http\Request;

class ReleaseRepository implements ReleaseRepositoryInterface
{
    public function all()
    {
        $listReleaseNumber =  Realse::with('image')->paginate(3);
        if ($listReleaseNumber) {
            return response()->json([
                'pagination' => [
                    'total' => $listReleaseNumber->total(),
                    'per_page' => $listReleaseNumber->perPage(),
                    'current_page' => $listReleaseNumber->currentPage(),
                    'last_page' => $listReleaseNumber->lastPage(),
                    'from' => $listReleaseNumber->firstItem(),
                    'to' => $listReleaseNumber->lastItem()
                ],
                'data' => $listReleaseNumber
            ]);
        }else {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => 'true',
                    'code' => 400,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
        return $listReleaseNumber;
    }
    public function create(array $input){
        $releaseNumber = Realse::create($input);
        return $releaseNumber;
    }

    public function storage($file) {
        return $name = time().$file->getClientOriginalName();
    }

    public function find($id)
    {
        // TODO: Implement find() method.
    }

    public function edit($id) {
        $realse = Realse::with('image')->find($id);
        return $realse;
    }

    public function update(array $input,$id) {
        $release = Realse::find($id);
        $release->update($input);
        return $release;
    }

    public function delete($id)
    {
        $release = Realse::find($id);
        return $release->delete($id);
    }
}