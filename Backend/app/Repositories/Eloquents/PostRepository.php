<?php
namespace Backend\Repositories\Eloquents;
use Backend\Repositories\Contracts\PostRepositoryInterface;
use Illuminate\Http\Request;
use Backend\Models\Post;

class PostRepository implements PostRepositoryInterface
{
    public function get_all() {
        $posts = Post::with('category.parent','release_number','image','user')->paginate(3);
        return $posts;
    }

    public function create(array $input) {
        $post = Post::create($input);
        return $post;
    }

    public function edit($id)
    {
        $post = Post::with('category.parent','release_number','image','user')->find($id);
        return $post;
    }

    public function update(array $input, $id)
    {
        $post = Post::find($id);
        $post->update($input);
        return $post;
    }
    public function delete($id)
    {
        $post = Post::find($id);
        return $post->delete($id);
    }
}