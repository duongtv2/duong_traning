<?php
namespace Backend\Repositories\Eloquents;
use Backend\Repositories\Contracts\CategoryRepositoryInterface;
use Illuminate\Http\Request;
use Backend\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function create(array $input)
    {
        return $category = Category::create($input);
    }
    public function get_all()
    {
        return $categories = Category::with('child')->where('parent_id',0)->get();
    }

    public function getbyid($id) {
        return $categories_child = Category::where('parent_id',$id)->get();
    }
}