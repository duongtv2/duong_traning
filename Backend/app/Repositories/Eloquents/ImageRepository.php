<?php
// app/Repositories/Eloquents/ProductRepository.php

namespace Backend\Repositories\Eloquents;
use Backend\Repositories\Contracts\ImageRepositoryInterface;
use Illuminate\Http\Request;
use Backend\Models\Image;
use Illuminate\Support\Facades\Storage;

class ImageRepository implements ImageRepositoryInterface
{
    public function create(array $input){
        return Image::create($input);
    }
    public function find($id){
        return Image::find($id);
    }
    public function update(array $input, $id) {
        $image = Image::find($id);
        $this->deleteStorage($image);
        $image->update($input);
        return $image;
    }
    public function delete($id)
    {
        $image = Image::find($id);
        if($image->delete($id)) {
            $this->deleteStorage($image);
        }
    }

    public function deleteStorage($image) {
        $image_name = $image->name;
        $url = '/public/' . $image_name;
        Storage::delete($url);
    }
}