<?php
// app/Repositories/Redis/RedisProductRepository.php

namespace App\Repositories\Redis;

use Backend\Repositories\Contracts\ReleaseRepositoryInterface;

class RedisReleaseRepository implements ReleaseRepositoryInterface
{
    public function all()
    {
        return 'Get all product from Redis';
    }

    public function find($id)
    {
        return 'Get single product by id: ' . $id;
    }
}