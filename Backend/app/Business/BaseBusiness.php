<?php

namespace Backend\Business;

//use Backend\Common\Constant;

class BaseBusiness
{
    public function returnFail($message, $code)
    {
        return [
            'data' => null,
            'message' => $message,
            'code' => $code
        ];
    }

    public function returnSuccess($data = null)
    {
        return [
            'data' => $data,
            'message' => __('auth.success'),
            'code' => 200
        ];
    }
}
