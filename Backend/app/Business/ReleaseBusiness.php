<?php
namespace Backend\Business;
use Backend\Repositories\Contracts\ImageRepositoryInterface;
use Backend\Repositories\Contracts\ReleaseRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ReleaseBusiness extends BaseBusiness
{
    protected $releaseRepository;
    protected $imageRepository;
    protected $releaseBusiness;
    public function __construct(ReleaseRepositoryInterface $releaseRepository, ImageRepositoryInterface $imageRepository)
    {
        $this->releaseRepository = $releaseRepository;
        $this->imageRepository = $imageRepository;
    }
    public function listReleaseNumber() {
        $listReleaseNumber = $this->releaseRepository->all();
        return $listReleaseNumber;
    }
    public function createRelaseNumber(Request $request) {
        try{
            DB::beginTransaction();
            if($request->file('image')) {
                $file = $request->file('image');
                $img_name = time() . $file->getClientOriginalName();
                Storage::disk('public')->put($img_name, File::get($file));
            }
            $input_image = [
                'name' => $img_name
            ];
            $image = $this->imageRepository->create($input_image);
            $input_release_number = [
                'name' => $request->name,
                'description' => $request->description,
                'image_id' => $image->id
            ];
            $realseNumber = $this->releaseRepository->create($input_release_number);
            DB::commit();
            return $realseNumber;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
    public function updateReleaseNumber(Request $request, $id) {
        try{
            DB::beginTransaction();
            $file = $request->file('image');
            $img_name = time() . $file->getClientOriginalName();
            Storage::disk('public')->put($img_name, File::get($file));
            $image_id = $request->image_id;
            $imageData = [
                'name' => $img_name
            ];
            $image = $this->imageRepository->update($imageData,$image_id);
            $relase_input = [
                'name' => $request->name,
                'description' => $request->description,
                'image_id' =>  $request->image_id,
                'price' => 0
            ];
            $realseNumber = $this->releaseRepository->update($relase_input, $id);
            DB::commit();
            return $realseNumber;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
    public function destroyReleaseNumber($id) {
        try{
            DB::beginTransaction();
            $release_number = $this->releaseRepository->edit($id);
            $image_id = $release_number->image->id;
            if($this->releaseRepository->delete($id)) {
                $this->imageRepository->delete($image_id);
            }
            DB::commit();
            return $release_number;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
}