<?php
namespace Backend\Business;
use Backend\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryBusiness extends BaseBusiness
{
    protected $categoryRepository;
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function listCategory() {
        $listCategories = $this->categoryRepository->get_all();
        return $listCategories;
    }
    public function createCategory($request) {
        $category_input = [
            'name' => $request->name,
            'display_global' => 0,
            'display_menubar' => 0,
            'parent_id' => $request->parent_id
        ];
        return $this->categoryRepository->create($category_input);
    }
}