<?php

namespace Backend\Business;

use Backend\Repositories\Contracts\CategoryRepositoryInterface;
use Backend\Repositories\Contracts\ImageRepositoryInterface;
use Backend\Repositories\Contracts\PostRepositoryInterface;
use Backend\Repositories\Contracts\ReleaseRepositoryInterface;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Backend\Models\Post;

class PostBusiness extends BaseBusiness
{
    private $releaseRepository;
    private $imageRepository;
    private $postRepository;
    private $categoryRepository;

    public function __construct(ReleaseRepositoryInterface $releaseRepository, ImageRepositoryInterface $imageRepository, CategoryRepositoryInterface $categoryRepository, PostRepositoryInterface $postRepository)
    {
        $this->releaseRepository = $releaseRepository;
        $this->imageRepository = $imageRepository;
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function createPost(Request $request)
    {
        try {
            DB::beginTransaction();
            $file = $request->file('image');
            $img_name = time() . $file->getClientOriginalName();
            Storage::disk('public')->put($img_name, File::get($file));
            $imageData = [
                'name' => $img_name
            ];
            $image = $this->imageRepository->create($imageData);
            $createData = [
                'title' => $request->title,
                'release_date' => $request->release_date,
                'content' => $request->content,
                'status' => $request->status,
                'release_number_id' => $request->release_number_id,
                'user_id' => $request->user_id,
                'category_id' => $request->category_id,
                'image_id' => $image->id
            ];
            $post = $this->postRepository->create($createData);
            DB::commit();
            return $post;
        } catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
    public function deletePost($id) {
        try {
            $post = Post::with('image')->find($id);
            $image_id = $post->image->id;
            $this->postRepository->delete($id);
            $this->imageRepository->delete($image_id);
            DB::commit();
            return $post;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
    public function updatePost(Request $request, $id) {
        try{
            DB::beginTransaction();
            $file = $request->file('image');
            $img_name = time() . $file->getClientOriginalName();
            Storage::disk('public')->put($img_name, File::get($file));
            $image_id = $request->image_id;
            $imageData = [
                'name' => $img_name
            ];
            $image = $this->imageRepository->update($imageData,$image_id);
            $editData = [
                'title' => $request->title,
                'release_date' => $request->release_date,
                'content' => $request->content,
                'status' => $request->status,
                'release_number_id' => $request->release_number_id,
                'user_id' => $request->user_id,
                'category_id' => $request->category_id,
            ];
            $post = $this->postRepository->update($editData,$id);
            DB::commit();
            return $post;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
}