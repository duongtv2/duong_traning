<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReleaseNumberRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => __('validate.release_number.name.required'),
            'name.max' => __('validate.release_number.name.max'),
            'description.require' => __('validate.release_number.description.required'),
            'description.max' => __('validate.release_number.description.max'),
        ];
    }
}
