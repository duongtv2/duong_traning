<?php

namespace Backend\Http\Requests;


use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

trait RequestTrait
{
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'data' => null,
            'error' => [
                'status' => 422,
                'code' => 422,
                'message' => $validator->errors(),
            ]
        ], 422));
    }
}
