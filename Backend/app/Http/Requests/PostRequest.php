<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'title' => 'required|min:10|max:300',
            'release_date' => 'required|max:14',
            'content' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => __('validate.post.name.required'),
            'name.min' => __('validate.post.name.min'),
            'name.max' => __('validate.post.name.max'),
            'release_date.required' => __('validate.post.release_date.required'),
            'release_date.max' => __('validate.post.release_date.max'),
            'content.required' => __('validate.post.content.required'),
        ];
    }
}
