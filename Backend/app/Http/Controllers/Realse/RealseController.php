<?php

namespace Backend\Http\Controllers\Realse;

use Backend\Business\ReleaseBusiness;
use Backend\Http\Requests\ReleaseNumberRequest;
use Backend\Repositories\Contracts\ImageRepositoryInterface;
use Backend\Repositories\Contracts\ReleaseRepositoryInterface;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;
use Backend\Models\Realse;
use Backend\Repositories\Eloquents\ReleaseRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Backend\Models\Image;
use Illuminate\Support\Carbon;

class RealseController extends Controller
{
    protected $realseRepository;
    protected $imageRepository;
    protected $releaseBusiness;
    private $name_image;

    public function __construct(ReleaseRepositoryInterface $realseRepository, ImageRepositoryInterface $imageRepository, ReleaseBusiness $releaseBusiness)
    {
        $this->realseRepository = $realseRepository;
        $this->imageRepository = $imageRepository;
        $this->releaseBusiness = $releaseBusiness;
    }

    public function index()
    {
        $listReleaseNumber = $this->releaseBusiness->listReleaseNumber();
        abort_if(is_null($listReleaseNumber), 500);
        return $listReleaseNumber;
    }

    public function create(ReleaseNumberRequest $request)
    {
        $releaseNumber = $this->releaseBusiness->createRelaseNumber($request);
        abort_if(is_null($releaseNumber), 500);
        return $releaseNumber;
    }


    public function storage(Request $request) {

    }

    public function edit(Request $request, $id) {
        return $this->realseRepository->edit($id);
    }

    public function update(ReleaseNumberRequest $request, $id) {
        $releaseNumber = $this->releaseBusiness->updateReleaseNumber($request, $id);
        abort_if(is_null($releaseNumber), 500);
        return $releaseNumber;
    }
    public function destroy($id) {
        $release_number = $this->releaseBusiness->destroyReleaseNumber($id);
        abort_if(is_null($release_number), 500);
        return $release_number;
    }

}
