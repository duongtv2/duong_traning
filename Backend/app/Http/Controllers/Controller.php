<?php

namespace Backend\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response ($message, $code, $data = null) {
        $status = $code !== 200;
        return response()->json([
            'data' => $data,
            'error' => [
                'status' => $status,
                'code' => $code,
                'message' => $message,
            ]
        ], $code);
    }

    public function responsePagination($data, $message, $code, $paginate)
    {
        $status = $code !== 200;

        return response()->json([
            'pagination' => $paginate,
            'data' => $data,
            'error' => [
                'status' => $status,
                'code' => $code,
                'message' => $message,
            ]
        ], $code);
    }
}
