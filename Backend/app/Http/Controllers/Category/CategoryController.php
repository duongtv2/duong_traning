<?php

namespace Backend\Http\Controllers\Category;

use Backend\Business\CategoryBusiness;
use Backend\Repositories\Eloquents\CategoryRepository;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $categoryRepository;
    protected $categoryBusinesss;
    public function __construct(CategoryRepository $categoryRepository, CategoryBusiness $categoryBusinesss)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryBusinesss = $categoryBusinesss;
    }

    public function index() {
        $listCategories = $this->categoryBusinesss->listCategory();
        abort_if(is_null($listCategories), 500);
        return $listCategories;
    }

    public function create(Request $request) {
        $category = $this->categoryBusinesss->createCategory($request);
        abort_if(is_null($category), 500);
        return $category;
    }

    public function show($id) {
        return $category_child = $this->categoryRepository->getbyid($id);
    }
}
