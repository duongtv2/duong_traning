<?php

namespace Backend\Http\Controllers\Auth;


use Backend\Http\Controllers\Controller;


use Backend\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends Controller
{

    public function login(Request $request)
    {
        $credentials = array(
            'email' => $request->email,
            'password' => $request->password,
        );

        try {
            $token = JWTAuth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'message' => "login sai"
                    ]
                ]);

            } else {
                $user = User::where('email', $request->email)->first();
                if ($user->status == 0) {
                    return response()->json([
                        'data' => false,
                        'error' => [
                            'status' => true,
                            'message' => "khong hoat dong"
                        ]
                    ]);
                }
                return response()->json([
                    'data' => $token,
                    'error' => [
                        'status' => false,
                        'message' => "login thanh cong"
                    ]
                ]);
            }
        } catch (JWTException $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => "loi he thong"
                ]
            ]);
        }
    }

    public function getAuthenticatedUser()
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return view('index');
        }
        $ar_user = compact('user')['user'];
        $user = array(
            'id' => $ar_user['id'],
            'name' => $ar_user['name'],
            'email' => $ar_user['email'],
            'role' => $ar_user['role']
        );
        return response()->json([
            'data' => $user,
            'error' => [
                'status' => false,
            ]
        ]);
    }

}
