<?php

namespace Backend\Http\Controllers\Post;

use Backend\Business\PostBusiness;
use Backend\Http\Requests\PostRequest;
use Backend\Models\Image;
use Backend\Models\Post;
use Backend\Models\Realse;
use Backend\Repositories\Contracts\CategoryRepositoryInterface;
use Backend\Repositories\Contracts\ImageRepositoryInterface;
use Backend\Repositories\Contracts\PostRepositoryInterface;
use Backend\Repositories\Contracts\ReleaseRepositoryInterface;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    protected $realseRepository;
    protected $imageRepository;
    protected $postRepository;
    protected $categoryRepository;
    protected $postBusiness;

    public function __construct(
        ReleaseRepositoryInterface $realseRepository, ImageRepositoryInterface $imageRepository, CategoryRepositoryInterface $categoryRepository, PostRepositoryInterface $postRepository, PostBusiness $postBusiness
    )
    {
        $this->realseRepository = $realseRepository;
        $this->imageRepository = $imageRepository;
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->postBusiness = $postBusiness;
    }

    public function index()
    {
        return $this->postRepository->get_all();
    }

    public function create(PostRequest $request)
    {
        $post = $this->postBusiness->createPost($request);
        abort_if(is_null($post), 500);
        return $post;
    }

    public function edit(Request $request, $id)
    {
        $post = $this->postRepository->edit($id);
        return $post;
    }

    public function update(Request $request, $id)
    {
        $post = $this->postBusiness->updatePost($request, $id);
        abort_if(is_null($post), 500);
        return $post;
    }

    public function destroy($id)
    {
        $post = $this->postBusiness->deletePost($id);
        abort_if(is_null($post), 500);
        return $post;
    }
}
