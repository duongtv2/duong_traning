require('./bootstrap')
window.Vue = require('vue')
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import router from './router'
import App from './pages/App.vue'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

// axios.defaults.headers.post['Content-type'] = 'application/x-www-form-urlencoded';

const app = new Vue({
    el: '#app',
    components: { App },
    mode: 'history',
    router
});
