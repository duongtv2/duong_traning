import VueRouter from 'vue-router'
// import UserList from './components/admin/UserComponent'
// import CatList from './components/admin/CatComponent'
// import AdminLayout from './layouts/layout'
// import LoginLayout from './pages/admin/Login/Login'
import LoginLayout from './pages/admin/Login/Login'
import AdminLayout from './layouts/layout'
import Test from './pages/admin/Test/'
import Relase from './pages/admin/Relase/index'
import RelaseCreateComponent from './pages/admin/Relase/create'
import RelaseEditComponent from './pages/admin/Relase/edit'
import PostComponent from './pages/admin/Post/index'
import PostCreateComponent from './pages/admin/Post/create'
import PostEditComponent from './pages/admin/Post/edit'
const routes = [
    {
        path: "/",
        component: LoginLayout,
        mode: 'history',
        children: [
            {
                path: '/login',
                component: LoginLayout,
                name: 'users.index',
            },
        ]
    },
    {
        path: "/home",
        component: AdminLayout,
        children: [
            {
                path:'/home',
                redirect : '/release_number'
            },
            {
                path: '/release_number',
                component: Relase,
                name: 'release_number-index',
            },
            {
                path: '/release_number/create',
                component: RelaseCreateComponent,
                name: 'create-release_number',
            },
            {
                path: '/release_number/edit/:id',
                component: RelaseEditComponent,
                name: 'create-edit',
            },
            {
                path: '/post',
                component: PostComponent,
                name: 'post-index',
            },
            {
                path: '/post/create',
                component: PostCreateComponent,
                name: 'post-create',
            },
            {
                path: '/post/edit/:id',
                component: PostEditComponent,
                name: 'post-edit',
            },
        ]
    }
];

var router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    var access_token = localStorage.getItem('access_token');
    if (to.path !== '/' && !access_token) {
        next('/login');
    } else if (to.path === '/' && access_token) {
        next('/release_number');
    }else if (to.path === '/login' && access_token) {
        next('/release_number');
    } else {
        next();
    }
});



export default router;