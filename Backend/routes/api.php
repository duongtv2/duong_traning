<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', 'Auth\AuthController@login');
Route::get('/release_number','Realse\RealseController@index');
Route::post('release_number/create','Realse\RealseController@create');
Route::post('storage','Realse\RealseController@storage');
Route::get('release_number/edit/{id}','Realse\RealseController@edit');
Route::post('release_number/update/{id}','Realse\RealseController@update');
Route::delete('release_number/delete/{id}','Realse\RealseController@destroy');
Route::get('posts','Post\PostController@index');
Route::post('posts/create','Post\PostController@create');
Route::get('categories','Category\CategoryController@index');
Route::post('categories/create','Category\CategoryController@create');
Route::get('categories/{id}','Category\CategoryController@show');
Route::get('posts/edit/{id}','Post\PostController@edit');
Route::patch('posts/update/{id}','Post\PostController@update');
Route::delete('posts/delete/{id}','Post\PostController@destroy');
Route::get('auth_user','Auth\AuthController@getAuthenticatedUser');