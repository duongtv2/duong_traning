<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Frontend\Repositories\Contracts\PostRepositoryInterface',
            'Frontend\Repositories\Eloquents\PostRepository'
        );
        $this->app->bind(
            'Frontend\Repositories\Contracts\ReleaseNumberRepositoryInterface',
            'Frontend\Repositories\Eloquents\ReleaseNumberRepository',
            'Frontend\Business\ReleaseNumberBusiness'
        );
        $this->app->bind(
            'Frontend\Repositories\Contracts\UserRepositoryInterface',
            'Frontend\Repositories\Eloquents\UserRepository',
            'Frontend\Business\UserBusiness'
        );
    }
}
