<?php
namespace Frontend\Business;

use Frontend\Repositories\Eloquents\CategoryRepository;

class CategoryBusiness extends BaseBusiness {
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
 * Check validate login
 * @param Get all the cagegory displayed on the menu
 * @return object
 */
    public function listMenu() {
        $categoryMenu = $this->categoryRepository->listMenu();
        return $categoryMenu;
    }

    public function listGlobal() {
        $categoryGlobal = $this->categoryRepository->listGlobal();
        return $categoryGlobal;
    }
}