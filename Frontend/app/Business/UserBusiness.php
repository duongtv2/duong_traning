<?php
namespace Frontend\Business;

use Frontend\Http\Requests\RegisterRequest;
use Frontend\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class UserBusiness extends BaseBusiness {
    protected $userRepositoy;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepositoy = $userRepository;
    }

    /**
     * Get list releaseNumber
     * @param
     * @return object
     */
    public function register(RegisterRequest $request) {
        try{
            DB::beginTransaction();
            $register_input = [
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'birth_day' => $request->birthday,
                'gender' => $request->gender,
                'address' => '',
                'phone' => '',
                'status' => 1,
                'role_id' => 1,
            ];
            $user = $this->userRepositoy->register($register_input);
            DB::commit();
            return $user;
        }catch (\Exception $e) {
            DB::rollBack();
            return null;
        }
    }
}
