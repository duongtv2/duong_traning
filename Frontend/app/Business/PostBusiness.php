<?php
namespace Frontend\Business;
use Frontend\Repositories\Contracts\PostRepositoryInterface;

class PostBusiness extends BaseBusiness {
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Get the page post
     * @param $page
     * @return object
     */
    public function listPost($page) {
        $posts =  $this->postRepository->listPost($page);
        return $posts;
    }

    /**
     * Get the latest 4 post
     * @param
     * @return object
     */
    public function listPostNew() {
        $posts = $this->postRepository->listPostNew();
        return $posts;
    }

    public function showPost($id) {
        $post = $this->postRepository->showPost($id);
        return $post;
    }

    public function listPostByRelease($page, $id) {
        $posts = $this->postRepository->listPostByRelease($page,$id);
        return $posts;
    }
}