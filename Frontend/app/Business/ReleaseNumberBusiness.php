<?php
namespace Frontend\Business;
use Frontend\Business\BaseBusiness;
use Frontend\Repositories\Contracts\ReleaseNumberRepositoryInterface;

class ReleaseNumberBusiness extends BaseBusiness {

    protected $releaseNumberRepository;

    public function __construct(ReleaseNumberRepositoryInterface $releaseNumberRepository)
    {
        $this->releaseNumberRepository = $releaseNumberRepository;
    }

    /**
     * Get list releaseNumber
     * @param
     * @return object
     */
    public function listReleaseNumber() {
        $listReleaseNumber = $this->releaseNumberRepository->listReleaseNumber();
        return $listReleaseNumber;
    }

    public function showReleaseNunber($id) {
        $releaseNumber = $this->releaseNumberRepository->show($id);
        return $releaseNumber;
    }
}