<?php

namespace Frontend\Models;

use Frontend\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;
    protected $table = 'posts';
    protected $fillable = [
        'title','release_date','content','status','release_number_id','user_id','category_id','image_id'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function release_number() {
        return $this->belongsTo(Release::class);
    }

    public function image() {
        return $this-> belongsTo(Image::class);
    }

    public function user() {
        return $this-> belongsTo(User::class);
    }
}
