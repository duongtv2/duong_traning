<?php

namespace Frontend\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'name'
    ];

    public function realse() {
        return $this->hasOne('Frontend\Models\Realse');
    }

    public function post() {
        return $this->hasOne(Post::class);
    }
}
