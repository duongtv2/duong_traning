<?php

namespace Frontend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;
    protected $table = 'categories';
    protected $fillable = [
        'name','display_global','display_menubar','parent_id'
    ];

    public function parent() {
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function childs() {
        return $this->hasMany(Category::class,'parent_id');
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }
}
