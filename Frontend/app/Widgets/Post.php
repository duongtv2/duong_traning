<?php

namespace Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Business\PostBusiness;

class Post extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $postBusinuess;

    /**
     * initialize constructor function
     * @param
     * @return
     */
    public function __construct(PostBusiness $postBusinuess, $config)
    {
        $this->postBusinuess = $postBusinuess;
        $this->config = $config;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */

    public function run()
    {
        $posts = $this->postBusinuess->listPostNew();
        return view('widgets.post', [
            'config' => $this->config,
            'posts' => $posts
        ]);
    }
}
