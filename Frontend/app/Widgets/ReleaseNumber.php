<?php

namespace Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Business\ReleaseNumberBusiness;
use Frontend\Repositories\Contracts\ReleaseNumberRepositoryInterface;

class ReleaseNumber extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $relaseNumberBusiness;

    public function __construct(ReleaseNumberBusiness $releaseNumberBusiness,$config)
    {
        $this->relaseNumberBusiness = $releaseNumberBusiness;
        $this->config = $config;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $releaseNumber = $this->relaseNumberBusiness->listReleaseNumber();
        return view('widgets.release_number', [
            'config' => $this->config,
            'releaseNumber' => $releaseNumber
        ]);
    }
}
