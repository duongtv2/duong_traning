<?php

namespace Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Business\CategoryBusiness;

class globalBar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $categoryBusiness;

    /**
     * initialize constructor function
     * @param
     * @return
     */
    public function __construct(CategoryBusiness $categoryBusiness, $config)
    {
        $this->categoryBusiness = $categoryBusiness;
        $this->config = $config;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $globalCategory = $this->categoryBusiness->listGlobal();
        return view('widgets.global_bar', [
            'config' => $this->config,
            'globalCategory' => $globalCategory
        ]);
    }
}
