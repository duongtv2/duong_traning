<?php
namespace Frontend\Repositories\Contracts;

interface PostRepositoryInterface
{
    public function listPost($page);
    public function listPostNew();
    public function showPost($id);
    public function listPostByRelease($page,$id);
}