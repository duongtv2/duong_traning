<?php
namespace Frontend\Repositories\Contracts;

interface CategoryRepositoryInterface {
    public function listMenu();
    public function listGlobal();
}
