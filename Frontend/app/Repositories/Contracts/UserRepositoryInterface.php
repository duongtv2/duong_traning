<?php

namespace Frontend\Repositories\Contracts;

interface UserRepositoryInterface
{
    public function register(array $input);
}
