<?php
namespace Frontend\Repositories\Contracts;

interface ReleaseNumberRepositoryInterface
{
    public function listReleaseNumber();
    public function show($id);
}