<?php
namespace Frontend\Repositories\Eloquents;

use Frontend\Repositories\Contracts\UserRepositoryInterface;
use Frontend\User;

class UserRepository implements UserRepositoryInterface {
    public function register(array $input)
    {
        $user = User::create($input);
        return $user;
    }
}