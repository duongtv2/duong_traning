<?php
namespace Frontend\Repositories\Eloquents;
use Frontend\Models\Release;
use Frontend\Repositories\Contracts\ReleaseNumberRepositoryInterface;

class ReleaseNumberRepository implements ReleaseNumberRepositoryInterface {
    /**
     * Get list releaseNumber
     * @param
     * @return object
     */
    public function listReleaseNumber()
    {
        $listReleaseNumber = Release::with('image')->get();
        return $listReleaseNumber;
    }

    public function show($id)
    {
        $releaseNumber = Release::with('image')->find($id);
        return $releaseNumber;
    }
}