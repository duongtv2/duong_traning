<?php
namespace Frontend\Repositories\Eloquents;
use Frontend\Models\Post;
use Frontend\Repositories\Contracts\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    /**
     * Get the page post
     * @param $page
     * @return object
     */
    public function listPost($page)
    {
        $latestPosts = $this->listPostNew()->toArray();
        $latestPostId = array();
        foreach ($latestPosts as $latestPost) {
            $latestPostId[] = $latestPost['id'];
        }
        $posts = Post::with('category.parent','release_number','image','user')->where('status', '1')->whereNotIn('id', $latestPostId)->paginate(4);
        return $posts;
    }

    /**
     * Get the latest 4 post
     * @param
     * @return object
     */
    public function listPostNew()
    {
        $posts = Post::with('category.parent','release_number','image','user')->where('status', '1')->latest()->limit(4)->get();
        return $posts;
    }

    public function showPost($id)
    {
        $post = Post::with('category.parent','release_number','image','user')->find($id);
        return $post;
    }

    public function listPostByRelease($page, $id)
    {
        $posts = Post::with('category.parent','release_number','image','user')->where('release_number_id',$id)->latest()->paginate(4);
        return $posts;
    }
}