<?php
namespace Frontend\Repositories\Eloquents;
use Frontend\Models\Category;
use Frontend\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface {

    /**
     * Check validate login
     * @param Get all the cagegory displayed on the menu
     * @return object
     */
    public function listMenu()
    {
        $categoryMenu = Category::where('display_menubar','1')->get();
        return $categoryMenu;
    }

    public function listGlobal()
    {
        $categoryGlobal = Category::where('display_global','1')->get();
        return $categoryGlobal;
    }
}
