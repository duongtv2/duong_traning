<?php

namespace Frontend\Http\Controllers\Ajax;

use Frontend\Business\PostBusiness;
use Frontend\Models\Release;
use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $postBusiness;

    public function __construct(PostBusiness $postBusiness)
    {
        $this->postBusiness = $postBusiness;
    }

    public function loadMore(Request $request) {
        $page = $request->page;
        $posts = $this->postBusiness->listPost($page);
        return $posts;
    }

    public function releaseLoadMore(Request $request) {
        $page = $request->page;
        $id = $request->id;
        $posts = $this->postBusiness->listPostByRelease($page,$id);
        return $posts;
    }
}
