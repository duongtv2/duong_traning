<?php

namespace Frontend\Http\Controllers\ReleaseNumber;

use Frontend\Business\ReleaseNumberBusiness;
use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;

class ReleaseNumberController extends Controller
{
    protected $releaseNumberBusiness;

    public function __construct(ReleaseNumberBusiness $releaseNumberBusiness)
    {
        $this->releaseNumberBusiness = $releaseNumberBusiness;
    }

    public function showReleaseNunber($id) {
        $releaseNumber = $this->releaseNumberBusiness->showReleaseNunber($id);
        return view('release_number_detail',compact('releaseNumber'));
    }
}
