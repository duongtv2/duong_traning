<?php

namespace Frontend\Http\Controllers\Post;

use Frontend\Business\PostBusiness;
use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $postBusiness;
    public function __construct(PostBusiness $postBusiness)
    {
        $this->postBusiness = $postBusiness;
    }

    public function show($id) {
        $post = $this->postBusiness->showPost($id);
        return view('post_detail', compact('post'));
    }
}
