<?php

namespace Frontend\Http\Controllers\User;

use Frontend\Business\UserBusiness;
use Frontend\Http\Requests\LoginRequest;
use Frontend\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $userBusiness;

    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    /**
     * function login
     * @param LoginRequest $request
     * @return object
     */
    public function login(LoginRequest $request) {
        $data = $request->only('email', 'password');
        if (Auth::attempt($data)) {
            return Auth::user();
        }else{
            return null;
        }
    }

    /**
     * function logout
     * @param
     * @return
     */
    public function logout() {
        Auth::logout();
    }

    /**
     * function register
     * @param RegisterRequest $request
     * @return object
     */
    public function register(RegisterRequest $request) {
        $user = $this->userBusiness->register($request);
        if($user) {
            Auth::login($user);
            return Auth::user();
        }else{
            return null;
        }
    }
}
