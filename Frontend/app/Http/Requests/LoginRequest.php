<?php

namespace Frontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'メール を入力してください。',
            'password.required' => 'パスワード を入力してください。',
//            'name.max' => __('validate.post.name.max'),
//            'release_date.required' => __('validate.post.release_date.required'),
//            'release_date.max' => __('validate.post.release_date.max'),
//            'content.required' => __('validate.post.content.required'),
        ];
    }
}
