<?php

namespace Frontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|regex:/^[\pL\s]+$/u',
            'email' => 'required|email|max:64|min:6|unique:users',
            'password' => 'required|string|min:6|max:72',
            'birthday' => 'nullable|date|before:tomorrow'
        ];
    }
    public function messages()
    {
        return [
//            'username.required' => 'ジューザ名を入力してください。',
//            'username.max' => 'ジューザ名は１００文字以下の有効です。',
//            'email.required' => 'メール を入力してください。',
//            'email.max' => 'メールは６４文字以下の有効です。',
//            'password.required' => 'パスワード を入力してください。',
//            'password.max' => 'パスワードは７２文字以下の有効です。'
            'username.regex' => 'ユーザー名形式が正しくありません',
            'username.required' => 'ユーザー名を入力してください。',
            'email.unique' => 'メールは既に存在します。',
            'email.email' => 'メールフォーマットが正しくありません。',
            'email.required' => 'メールを入力してください。',
            'email.max' => 'メールは6文字から６４文字まで入力してください。',
            'email.min' => 'メールは6文字から６４文字まで入力してください。',
            'password.max' => 'パスワードは6文字から７２文字まで入力してください。',
            'password.min' => 'パスワードは6文字から７２文字まで入力してください。',
            'password.required' => 'パスワードを入力してください。',
            'birthday.date' => '間違った日付形式。',
            'birthday.before' => '誕生日は将来の日にはできません。',
        ];
    }
}
