<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('loadpost','Ajax\PostController@loadMore');
Route::post('login','User\AuthController@login');
Route::get('logout','User\AuthController@logout');
Route::post('register','User\AuthController@register');
Route::get('/post/{id}','Post\PostController@show')->name('show-post');
Route::get('/release_number/{id}','ReleaseNumber\ReleaseNumberController@showReleaseNunber')->name('show-releaseNumber');
Route::get('release_number_post','Ajax\PostController@releaseLoadMore');