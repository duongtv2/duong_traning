<div class="modal fade login-modal" id="login-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ログイン</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <div class="div-close" aria-hidden="true">閉じる</div>
                </button>
            </div>
            <div class="modal-body">
                <form id="login-form" method="POST" action="">
                    @csrf
                    <input type="hidden" name="_token" value="">
                    <span class="invalid-feedback feedback-info" role="alert">
                        <strong></strong>
                    </span>
                    <div class="user-name">
                        <span>メールアドレス</span>
                        <input type="text" placeholder="メールの入力" name="email" class="form-control"
                               id="email-login" required autofocus>
                        <span class="invalid-feedback feedback-username" role="alert">
                            <strong></strong>
                        </span>
                    </div>
                    <div class="pass-word">
                        <span>パスワード</span>
                        <input type="password" placeholder="パスワードの入力" name="password" class="form-control"
                               id="password-login"
                               required>
                        <span class="invalid-feedback feedback-password" role="alert">
                            <strong></strong>
                        </span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button form="login-form" type="button" class="btn-primary btn-login">ログイン</button>
                <button type="button" class="btn-secondary" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $( ".btn-login" ).click(function() {
            var email = $('#email-login').val();
            var password = $('#password-login').val();
            $.ajax({
                url : 'http://usersite.local/login',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    email: email,
                    password: password
                },
                success : function (data) {
                    if(data) {
                        location.reload();
                    }
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    $('.invalid-feedback strong').text('');
                    $('.form-control').removeClass('is-invalid');
                    if (!!errors.email) {
                        $('.feedback-username strong').text(errors.email);
                        $('#email-login').addClass('is-invalid');
                    }
                    if (!!errors.password) {
                        $('.feedback-password strong').text(errors.password);
                        $('#password-login').addClass('is-invalid');
                    }
                }
            });
        });
        $('#logout').click(function () {
            event.preventDefault();
            $.ajax({
                url : 'http://usersite.local/logout',
                type: 'GET',
                success : function (data) {
                    location.reload();
                },
            });
        });
        $('.btn-submit-register').click(function () {
            var username = $('#name-register').val();
            var email = $("#email-register").val();
            var password = $("#password-register").val();
            var birthday = $("input[name=birthday]").val();
            var gender = $("input[name='gender']:checked").val();
            $.ajax({
                url : 'http://usersite.local/register',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    username: username,
                    email: email,
                    password: password,
                    birthday: birthday,
                    gender: gender,
                },
                success : function (data) {
                    if(data) {
                        location.reload();
                    }
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    console.log(errors);
                    $('.invalid-feedback strong').text('');
                    $('.form-control').removeClass('is-invalid');
                    if (!!errors.username) {
                        $('.feedback-name strong').text(errors.username);
                        $('#name-register').addClass('is-invalid');
                    }
                    if (!!errors.email) {
                        $('.feedback-username strong').text(errors.email);
                        $('#email-register').addClass('is-invalid');
                    }
                    if (!!errors.password) {
                        $('.feedback-password strong').text(errors.password);
                        $('#password-register').addClass('is-invalid');
                    }
                    if (!!errors.birthday) {
                        $('.feedback-birthday strong').text(errors.birthday);
                        $('#date').addClass('is-invalid');
                    }
                }
            });
        });
    });
</script>