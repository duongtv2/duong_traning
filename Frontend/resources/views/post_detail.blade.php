@extends('layouts.master')
@section('content')
<div class="detailed_post">
    <div class="content_title">
        <span class="item_title">{{$post->title}}</span>
    </div>
    <div class="item_content">{!!html_entity_decode($post->content)!!}</div>
    <p class="item_user_release_date">
        <span class="item_user">作成者：</span>{{$post->user->username}}
        <span class="mark">|</span>
        <span class="item_release_date">作成日：</span>{{date('Y年m月d日号', strtotime($post->release_date))}}
    </p>
</div>
@endsection