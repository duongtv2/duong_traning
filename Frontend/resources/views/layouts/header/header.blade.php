<div class="row header">
    <div class="col-sm-12 content_header_1">
        <div class="float-right modal-auth">
            @if (Auth::check())
                <a id="logout">ログアウト</a>
            @else
                <a data-toggle="modal" data-target="#register-modal">メンバー登録</a>
                <span>|</span>
                <a data-toggle="modal" data-target="#login-modal">ログイン</a>
            @endif
        </div>
    </div>
    @include('modal_login')
    @include('modal_register')
    <div class="col-sm-12 content_header_2">
        <div class="row content_item col-sm-11">
            <div class="col-sm-4 item">
                <p>総合ジャーナル</p>
            </div>
            <div class="col-sm-4 item">
                <form class="row form">
                    <input type="" name="" class="form-control col-sm-10" placeholder="検索内容の入力。。。">
                    <button class="btn btn-primary col-sm-2 btn_search">検索</button>
                </form>
            </div>
            <div class="col-sm-4 item form-group row select-size">
                <label>文字サイズ</label>
                <button class="btn button_page" data-size="13px">小</button>
                <button class="btn button_page" data-size="15px">中</button>
                <button class="btn button_page" data-size="17px">大</button>
            </div>
        </div>
    </div>
    <div class="content_header_3 col-sm-12 row">
        <ul>
            @widget('MenuBar')
        </ul>
    </div>
    <div class="content_header_4 col-sm-12 row">
        <ul>
            @widget('globalBar')
        </ul>
    </div>
</div>