<ul class="nav" id="list_parent">
    <li class="nav-item">
        <a class="nav-link active" href="/">ホーム</a>
    </li>
    @foreach($menuCategory as $category)
        @if( !count($category->childs))
            <li class="nav-item">
                <a class="nav-link" href="">{{$category->name}}</a>
            </li>
        @else
            <li class="nav-item dropdown">
                <a class="nav-link" href="">{{$category->name}}</a>
                <div class="dropdown-menu">
                    @foreach($category->childs as $child)
                        <a class="dropdown-item" href="">{{$child->name}}</a>
                    @endforeach
                </div>
            </li>
        @endif
    @endforeach
</ul>
