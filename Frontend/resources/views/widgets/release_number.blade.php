<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach($releaseNumber as $key => $release)
            @if($key == 0)
                <div class="carousel-item active">
                    <div class="content-item">
                        <div class="col-sm-5 content_top_1">
                            <h3>発売号</h3>
                            <a href="{{route('show-releaseNumber',$release->id)}}"><p>{{date("Y年m月d日号", strtotime($release->name))}}</p></a>
                            <div class="content_p">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                    no...</p>
                            </div>
                            <button class="arrow" href="#carouselExampleIndicators" role="button" data-slide="next">次</button>
                        </div>
                        <div class="col-sm-7 content_top_2">
                            <img src="http://imgt.taimienphi.vn/cf/Images/2018/ba/5/hinh-anh-buon-co-don-tam-trang-128.jpg">
                        </div>
                    </div>
                </div>
            @else
                <div class="carousel-item">
                    <div class="content-item">
                        <div class="col-sm-5 content_top_1">
                            <h3>発売号</h3>
                            <a href="{{route('show-releaseNumber',$release->id)}}"><p>{{date("Y年m月d日号", strtotime($release->name))}}</p></a>
                            <div class="content_p">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                    no...</p>
                            </div>
                            <button class="arrow" href="#carouselExampleIndicators" role="button" data-slide="next">次</button>
                        </div>
                        <div class="col-sm-7 content_top_2">
                            <img src="http://imgt.taimienphi.vn/cf/Images/2018/ba/5/hinh-anh-buon-co-don-tam-trang-128.jpg">
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
