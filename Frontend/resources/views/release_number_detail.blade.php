@extends('layouts.master')
@section('content')
    <div class="detailed_release_number">
        <div class="release_number">
            <div class="item_image_name">
                <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/106b6961-e661-4b6c-9a63-2f94aea39d91/dbs4rvp-4deb08fc-347d-4e35-ab3c-070afcc8bb88.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzEwNmI2OTYxLWU2NjEtNGI2Yy05YTYzLTJmOTRhZWEzOWQ5MVwvZGJzNHJ2cC00ZGViMDhmYy0zNDdkLTRlMzUtYWIzYy0wNzBhZmNjOGJiODguanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.N80WHhc75Dwx0NdOJbITVUj0gMccM8I2eBnoPBMUNIY"
                     alt="">
                <div class="item_name">
                    <h1 class="name">ggggg</h1>
                    <span att_id="{{$releaseNumber->id}}" id="att_id" style="display: none;"></span>
                </div>
            </div>
            <div class="item_title">
                <h2>fffff</h2>
            </div>
        </div>
        <div class="post_items">

        </div>
        <div class="show_more">
            <button class="btn_show_more">もっと見る</button>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/release_number_detail.js"></script>
@endsection
