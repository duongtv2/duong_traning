<div class="modal fade register-modal" id="register-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">新規メンバー登録</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <div class="div-close" aria-hidden="true">閉じる</div>
                </button>
            </div>
            <form id="register-form" action="" method="post">
                @csrf
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <div class="name">
                        <span>ユーザー名</span>
                        <input type="text" placeholder="ユーザー名の入力" name="name" class="form-control"
                               id="name-register" required
                               autofocus>
                        <br/>
                        <span class="invalid-feedback feedback-name" role="alert">
                                        <strong></strong>
                                    </span>
                    </div>
                    <div class="user-name">
                        <span>メールアドレス</span>
                        <input type="text" placeholder="メールの入力" name="email" class="form-control"
                               id="email-register" required>
                        <span class="invalid-feedback feedback-username" role="alert">
                                        <strong></strong>
                                    </span>
                    </div>
                    <div class="pass-word">
                        <span>パスワード</span>
                        <input type="password" placeholder="パスワードの入力" name="password" class="form-control"
                               id="password-register"
                               required>
                        <span class="invalid-feedback feedback-password" role="alert">
                                        <strong></strong>
                                    </span>
                    </div>
                    <div class="birth-day">
                        <span>生年月日</span>
                        <input type="text" placeholder="生年月日の入力" class="form-control" id="date"
                               name="birthday" class="datepicker">
                    </div>
                    <div class="gender">
                        <span>性別</span>
                        <div class="radio-gender">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label" for="inlineRadio1">男</label>
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1"
                                       value="1" checked>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label" for="inlineRadio2">女</label>
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2"
                                       value="2">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button form="register-form" type="button" class="btn-primary btn-submit-register">登録</button>
                <button type="button" class="btn-secondary" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>
