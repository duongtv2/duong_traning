@extends('layouts.master')
@section('content')
    <div class="main row">
        <div class="content_1 col-sm-11 row">
            @widget('ReleaseNumber')
        </div>
        <div class="content_2 col-sm-11 row">
            <div class="col-md-12 content_top">
                <h4>新しい記事</h4>
            </div>
            @widget('post')
        </div>
        <div class="content_3 col-sm-11 row">
            <div class="content_top">
                <h4>記事</h4>
            </div>
            <div class="content_bot row" id="resouce_content">
            </div>
        </div>
        <div class="col-sm-12 button">
            <button id="button_more">もっと見る</button>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/welcome.js"></script>
@endsection