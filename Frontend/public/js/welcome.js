$(document).ready(function () {
    var page = 1;
    call_api_post_by_page(page);
    $("#button_more").click(function () {
        page++;
        call_api_post_by_page(page);
    });

    function call_api_post_by_page(page) {
        $.ajax({
            url: 'http://usersite.local/loadpost',
            type: 'GET',
            data: {
                page: page,
            },
            success: function (data) {
                $.each(data.data, function (index, value) {
                    var html = '<div class="item col-sm-6">';
                    html += '        <img src="">';
                    html += '        <div class="item_bot">';
                    html += '            <h4>' + value.title + '</h4>';
                    html += '            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>';
                    html += '            <a class="view-detail" href="#"></a>';
                    html += '        </div>';
                    html += '    </div>';
                    $("#resouce_content").append(html);
                });
            }
        })
    }

    $('.page_1').click(function () {
        $('body').css("font-size", 10);
    });


    $('.button_page').click(function () {
        var font_size = $(this).attr('data-size');
        $('html').css('fontSize', font_size );
        createCookieFontSize(10, font_size);
    });

    if($.cookie('FontSize')) {
        $('html').css('fontSize', $.cookie('FontSize') );
    }

    /**
     * Hover Show|Hide dropdown category child
     */
    $('.nav-item.dropdown').hover(function () {
        show(this);
    }, function () {
        hide(this);
    });
});
function createCookieFontSize(timeout, fontsize) {
    $.cookie('FontSize', fontsize, {expires: timeout});
}

/**
 * Hover Show dropdown category child
 * @param element
 */
function show(element) {
    $(element).addClass('show');
    $(element).find('.dropdown-menu').addClass('show');
}

/**
 * Hover Hide dropdown category child
 * @param element
 */
function hide(element) {
    $(element).removeClass('show');
    $(element).find('.dropdown-menu').removeClass('show');
}